package fr.paladium.sephira.api;

import fr.paladium.sephira.api.connection.ClientInitializer;
import fr.paladium.sephira.api.listener.ChannelListener;
import fr.paladium.sephira.api.netty.PipelineUtils;
import fr.paladium.sephira.api.packet.IPacket;
import fr.paladium.sephira.api.packet.JsonPacket;
import fr.paladium.sephira.api.util.Util;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.util.internal.PlatformDependent;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SephiraApi {
   private EventLoopGroup eventLoops;
   private Channel listener;
   private List<ChannelListener> channelListeners = new ArrayList();
   private final String channel;
   private final String host;
   private final int port;
   private final String access;
   private boolean epoll = false;

   public SephiraApi(String channel, String host, int port, String access) {
      this.channel = channel;
      this.host = host;
      this.port = port;
      this.access = access;
      this.eventLoops = new NioEventLoopGroup();
   }

   public void connect() throws InterruptedException {
      this.checkEpoll();
      Bootstrap bootstrap = ((Bootstrap)((Bootstrap)((Bootstrap)((Bootstrap)(new Bootstrap()).channel(PipelineUtils.getChannel())).group(this.eventLoops)).handler(new ClientInitializer(this))).option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 5000)).remoteAddress(Util.getAddr(this.host + ":" + this.port));
      this.listener = bootstrap.connect(this.host, this.port).sync().channel();
      this.sendIdentifier();
   }

   private void sendIdentifier() {
      this.sendPacket((new JsonPacket("identifier")).add("channel", this.channel).add("access", this.access));
   }

   public void disconnect(String reason) {
      if (this.listener.isActive()) {
         this.listener.eventLoop().schedule(new Runnable() {
            public void run() {
               SephiraApi.this.listener.close();
            }
         }, 500L, TimeUnit.MILLISECONDS);
      }

   }

   private void checkEpoll() {
      if (!PlatformDependent.isWindows() && this.epoll) {
         PipelineUtils.epoll = Epoll.isAvailable();
      }

   }

   public void sendPacket(IPacket packet) {
      if (this.listener.isActive()) {
         this.listener.writeAndFlush(packet.toString());
      }

   }

   public List<ChannelListener> getChannelListeners() {
      return this.channelListeners;
   }

   public void registerChannelListener(ChannelListener listener) {
      this.channelListeners.add(listener);
   }

   public void setEpoll(boolean value) {
      this.epoll = value;
   }

   public boolean isActive() {
      return this.listener.isActive();
   }
}
