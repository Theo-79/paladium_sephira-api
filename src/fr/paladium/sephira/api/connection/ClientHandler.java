package fr.paladium.sephira.api.connection;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class ClientHandler extends SimpleChannelInboundHandler<String> {
   private PacketHandler handler;

   public ClientHandler(PacketHandler handler) {
      this.handler = handler;
   }

   public void handlerAdded(ChannelHandlerContext ctx) {
      if (this.handler != null) {
         this.handler.connected(ctx.channel());
      }

   }

   public void channelInactive(ChannelHandlerContext ctx) {
      if (this.handler != null) {
         this.handler.disconnected(ctx.channel());
      }

   }

   protected void channelRead0(ChannelHandlerContext ctx, String msg) {
      if (this.handler != null) {
         this.handler.handle(msg);
      }

   }

   public void exceptionCaught(ChannelHandlerContext ctx, Throwable exception) {
      if (ctx.channel().isActive()) {
         if (this.handler != null) {
            this.handler.exception(exception);
         }

         ctx.close();
      }

   }
}
