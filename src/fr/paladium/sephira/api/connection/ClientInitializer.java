package fr.paladium.sephira.api.connection;

import fr.paladium.sephira.api.SephiraApi;
import fr.paladium.sephira.api.server.ServerHandler;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

public class ClientInitializer extends ChannelInitializer<SocketChannel> {
   private final SephiraApi sephira;

   public ClientInitializer(SephiraApi sephira) {
      this.sephira = sephira;
   }

   protected void initChannel(SocketChannel ch) throws Exception {
      ch.pipeline().addLast((String)"framer", (ChannelHandler)(new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter())));
      ch.pipeline().addLast((String)"decoder", (ChannelHandler)(new StringDecoder()));
      ch.pipeline().addLast((String)"encoder", (ChannelHandler)(new StringEncoder()));
      ch.pipeline().addLast((String)"handler", (ChannelHandler)(new ClientHandler(new ServerHandler(this.sephira))));
   }
}
