package fr.paladium.sephira.api.connection;

import fr.paladium.sephira.api.packet.IPacket;
import java.net.InetSocketAddress;
import org.json.simple.JSONObject;

public interface IConnection {
   String getChannel();

   String getAccessKey();

   void identifier(JSONObject var1);

   InetSocketAddress getAddress();

   void disconnect(String var1);

   void sendPacket(IPacket var1);
}
