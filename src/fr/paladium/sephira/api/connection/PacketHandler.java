package fr.paladium.sephira.api.connection;

import io.netty.channel.Channel;

public abstract class PacketHandler {
   public abstract String toString();

   public void connected(Channel ch) {
   }

   public void disconnected(Channel ch) {
   }

   public void handle(String msg) {
   }

   public void exception(Throwable exception) {
   }
}
