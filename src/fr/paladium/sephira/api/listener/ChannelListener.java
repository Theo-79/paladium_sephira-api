package fr.paladium.sephira.api.listener;

import fr.paladium.sephira.api.connection.IConnection;
import org.json.simple.JSONObject;

public abstract class ChannelListener {
   public void connected(IConnection connection) {
   }

   public void disconnected(IConnection connection) {
   }

   public void handle(IConnection connection, JSONObject result) {
   }
}
