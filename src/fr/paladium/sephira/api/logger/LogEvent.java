package fr.paladium.sephira.api.logger;

import fr.paladium.sephira.api.util.Timestamper;
import java.io.IOException;

public class LogEvent {
   public final String identifier;
   public final SephiraLogger.LogType type;
   public final String body;

   public LogEvent(String identifier, SephiraLogger.LogType type, String body, String... args) {
      this.identifier = identifier;
      this.type = type;
      String b = body;

      for(int i = 0; i < args.length; ++i) {
         b = b.replace("{" + i + "}", args[i]);
      }

      this.body = !b.endsWith(System.getProperty("line.separator")) ? b + System.getProperty("line.separator") : b;
   }

   public void post(LogEventWriter writer) {
      if (this.type == SephiraLogger.LogType.ERROR) {
         System.err.print(this.toStringConsole());
      } else {
         System.out.print(this.toStringConsole());
      }

      try {
         writer.write(this);
      } catch (IOException var3) {
         var3.printStackTrace();
      }

   }

   public String toString() {
      return "[" + Timestamper.now() + " " + this.identifier.toUpperCase() + "/" + this.type.toString() + "] " + this.body;
   }

   public String toStringConsole() {
      return "[" + this.identifier.toUpperCase() + "/" + this.type.toString() + "] " + this.body;
   }
}
