package fr.paladium.sephira.api.logger;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.io.Writer;

public class LogEventWriter implements Closeable, Flushable {
   private final Writer writer;

   public LogEventWriter(Writer writer) {
      this.writer = writer;
   }

   public void write(LogEvent event) throws IOException {
      if (event != null) {
         this.writer.write(event.toString());
      }

   }

   public void write(String comment) throws IOException {
      if (comment != null) {
         this.writer.write(!comment.startsWith("#") ? "#" + comment : comment);
      }

   }

   public void flush() throws IOException {
      this.writer.flush();
   }

   public void close() throws IOException {
      this.writer.close();
   }
}
