package fr.paladium.sephira.api.logger;

import fr.paladium.sephira.api.util.Timestamper;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.BlockingQueue;

public class LogThread extends Thread {
   private final String identifier;
   private File logDir = null;
   private final BlockingQueue<LogEvent> queue;
   private final LogEventWriter writer;
   private final String format = (new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss")).format(new Date());

   public LogThread(String identifier, File logDir, BlockingQueue<LogEvent> queue) {
      this.queue = queue;
      this.identifier = identifier;
      this.logDir = logDir;
      this.setName(this.identifier + "-Log-Thread");
      if (this.logDir == null) {
         this.logDir = new File("logs");
         this.logDir.mkdirs();
      }

      File log = new File(this.logDir, this.identifier + "-log_" + this.format + ".txt");

      try {
         this.writer = new LogEventWriter(new FileWriter(log));
         this.writer.write("Generated on " + Timestamper.now() + System.getProperty("line.separator"));
         Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            public void run() {
               try {
                  LogThread.this.writer.close();
               } catch (IOException var2) {
                  var2.printStackTrace();
               }

            }
         }));
      } catch (IOException var6) {
         var6.printStackTrace();
         throw new RuntimeException("Couldn't create LogEventWriter");
      }
   }

   public void run() {
      while(true) {
         LogEvent next;
         try {
            next = (LogEvent)this.queue.take();
         } catch (InterruptedException var3) {
            Thread.currentThread().interrupt();
            return;
         }

         if (next != null) {
            next.post(this.writer);
         }
      }
   }
}
