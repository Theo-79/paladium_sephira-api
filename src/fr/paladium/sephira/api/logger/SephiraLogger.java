package fr.paladium.sephira.api.logger;

import java.io.CharArrayWriter;
import java.io.File;
import java.io.PrintWriter;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class SephiraLogger {
   private final String identifier;
   private File logDir = null;
   private boolean showDebug = false;
   private int debugLevel = 0;
   private final BlockingQueue<LogEvent> queue = new ArrayBlockingQueue(128);

   public SephiraLogger(String identifier) {
      this.identifier = identifier;
   }

   public SephiraLogger(String identifier, File logDir) {
      this.identifier = identifier;
      this.logDir = logDir;
      this.logDir.mkdirs();
   }

   public void start() {
      (new LogThread(this.identifier, this.logDir, this.queue)).start();
   }

   public void info(String message, String... args) {
      this.queue.offer(new LogEvent(this.identifier, SephiraLogger.LogType.INFO, message, args));
   }

   public void warn(String message, String... args) {
      this.queue.offer(new LogEvent(this.identifier, SephiraLogger.LogType.WARN, message, args));
   }

   public void warn(String message, Throwable throwable, String... args) {
      this.warn(message + "\n" + this.logStackTrace(throwable), args);
   }

   public void error(String message, String... args) {
      this.queue.offer(new LogEvent(this.identifier, SephiraLogger.LogType.ERROR, message, args));
   }

   public void error(String message, Throwable throwable, String... args) {
      this.error(message + "\n" + this.logStackTrace(throwable), args);
   }

   public void debug(String message, String... args) {
      if (this.showDebug && this.debugLevel >= 0) {
         this.queue.offer(new LogEvent(this.identifier, SephiraLogger.LogType.DEBUG, message, args));
      }

   }

   public void debug(String message, int level, String... args) {
      if (this.showDebug && this.debugLevel >= level) {
         this.debug(message, args);
      }

   }

   public void debug(String message, boolean force, String... args) {
      if (this.showDebug || force) {
         this.debug(message, args);
      }

   }

   public String logStackTrace(Throwable throwable) {
      CharArrayWriter writer = new CharArrayWriter();

      String var3;
      try {
         throwable.printStackTrace(new PrintWriter(writer));
         var3 = writer.toString();
      } finally {
         writer.close();
      }

      return var3;
   }

   public void setShowDebug(boolean value, int level) {
      this.debugLevel = level;
      this.showDebug = value;
   }

   public void setDebugLevel(int level) {
      this.debugLevel = level;
   }

   public int getDebugLevel() {
      return this.debugLevel;
   }

   public static enum LogType {
      INFO,
      WARN,
      ERROR,
      DEBUG;
   }
}
