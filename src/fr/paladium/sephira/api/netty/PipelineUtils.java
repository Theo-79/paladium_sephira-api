package fr.paladium.sephira.api.netty;

import io.netty.channel.Channel;
import io.netty.channel.ServerChannel;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.epoll.EpollSocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

public class PipelineUtils {
   public static boolean epoll = false;

   public static Class<? extends ServerChannel> getServerChannel() {
      return epoll ? EpollServerSocketChannel.class : NioServerSocketChannel.class;
   }

   public static Class<? extends Channel> getChannel() {
      return epoll ? EpollSocketChannel.class : NioSocketChannel.class;
   }
}
