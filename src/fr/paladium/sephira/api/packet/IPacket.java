package fr.paladium.sephira.api.packet;

public abstract class IPacket {
   public abstract String toString();
}
