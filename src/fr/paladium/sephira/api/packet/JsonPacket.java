package fr.paladium.sephira.api.packet;

import org.json.simple.JSONObject;

public class JsonPacket extends IPacket {
   private JSONObject identifierObj = new JSONObject();
   private final String identifier;

   public JsonPacket(String identifier) {
      this.identifier = identifier;
   }

   public JsonPacket add(String id, String value) {
      this.identifierObj.put(id, value);
      return this;
   }

   public String toString() {
      JSONObject json = new JSONObject();
      json.put(this.identifier, this.identifierObj);
      return json.toJSONString() + "\r\n";
   }
}
