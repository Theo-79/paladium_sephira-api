package fr.paladium.sephira.api.server;

import fr.paladium.sephira.api.SephiraApi;
import fr.paladium.sephira.api.connection.IConnection;
import fr.paladium.sephira.api.connection.PacketHandler;
import fr.paladium.sephira.api.listener.ChannelListener;
import fr.paladium.sephira.api.packet.IPacket;
import io.netty.channel.Channel;
import java.net.InetSocketAddress;
import java.util.Iterator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ServerHandler extends PacketHandler implements IConnection {
   private final SephiraApi sephira;
   private Channel ch;
   private JSONParser json = new JSONParser();

   public ServerHandler(SephiraApi sephira) {
      this.sephira = sephira;
   }

   public void connected(Channel ch) {
      this.ch = ch;
      Iterator var2 = this.sephira.getChannelListeners().iterator();

      while(var2.hasNext()) {
         ChannelListener serverHandler = (ChannelListener)var2.next();
         serverHandler.connected(this);
      }

   }

   public void disconnected(Channel ch) {
      Iterator var2 = this.sephira.getChannelListeners().iterator();

      while(var2.hasNext()) {
         ChannelListener serverHandler = (ChannelListener)var2.next();
         serverHandler.disconnected(this);
      }

   }

   public void handle(String msg) {
      JSONObject result = null;

      try {
         result = (JSONObject)this.json.parse(msg);
      } catch (ParseException var5) {
         return;
      }

      Iterator var3 = this.sephira.getChannelListeners().iterator();

      while(var3.hasNext()) {
         ChannelListener serverHandler = (ChannelListener)var3.next();
         serverHandler.handle(this, result);
      }

   }

   public void identifier(JSONObject result) {
   }

   public void disconnect(String reason) {
   }

   public void sendPacket(IPacket packet) {
   }

   public InetSocketAddress getAddress() {
      return (InetSocketAddress)this.ch.remoteAddress();
   }

   public String getChannel() {
      return null;
   }

   public String getAccessKey() {
      return null;
   }

   public String toString() {
      return this.getAddress().toString();
   }
}
