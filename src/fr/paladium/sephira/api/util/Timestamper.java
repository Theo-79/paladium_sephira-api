package fr.paladium.sephira.api.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Timestamper {
   private static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

   public static String now() {
      return format.format(new Date());
   }

   public static String was(Date date) {
      return format.format(date);
   }
}
