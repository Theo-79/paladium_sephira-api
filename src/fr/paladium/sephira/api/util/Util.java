package fr.paladium.sephira.api.util;

import java.io.File;
import java.io.FileInputStream;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.MessageDigest;

public class Util {
   public static void sleep(long millis) {
      try {
         Thread.sleep(millis);
      } catch (InterruptedException var3) {
      }

   }

   public static InetSocketAddress getAddr(String host) {
      URI uri;
      try {
         uri = new URI("tcp://" + host);
      } catch (URISyntaxException var3) {
         throw new IllegalArgumentException("Bad hostline", var3);
      }

      return new InetSocketAddress(uri.getHost(), uri.getPort());
   }

   public static String make_key(int length) {
      String result = "";
      String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for(int i = 0; i < length; ++i) {
         result = result + characters.charAt((int)Math.floor(Math.random() * (double)characters.length()));
      }

      return result;
   }

   public static String getMD5(File file) {
      if (!file.exists()) {
         return "0";
      } else {
         StringBuffer sb = null;

         try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            FileInputStream fis = new FileInputStream(file);
            byte[] dataBytes = new byte[1024];
            boolean var5 = false;

            int nread;
            while((nread = fis.read(dataBytes)) != -1) {
               md.update(dataBytes, 0, nread);
            }

            byte[] mdbytes = md.digest();
            sb = new StringBuffer();

            for(int i = 0; i < mdbytes.length; ++i) {
               sb.append(Integer.toString((mdbytes[i] & 255) + 256, 16).substring(1));
            }

            if (fis != null) {
               fis.close();
            }
         } catch (Exception var8) {
            return "0";
         }

         return sb.toString();
      }
   }
}
